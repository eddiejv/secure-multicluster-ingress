# Configuring single MultiClusterIngress with Cloud Armor protection to multiple GKE Services using ASM Ingress

This tutorial shows how to create a single MultiClusterIngress (i.e. GCLB frontend) with multiple Services (i.e. backends) running in multiple namespaces on multiple GKE clusters.  

```mermaid
graph TD;
subgraph GCP
subgraph mesh
    subgraph A[gke-west]
        subgraph Aistions[istio-system ns]
            Agateway([Gateway]) -.- Aingress[istio-ingressgateway]
        end
        subgraph Abankns[bank-of-anthos ns]
            Abankfrontendvs([VirtualService]) -.-|host based routing to Service| Abankfrontend[frontend]
        end
        subgraph Ashopns[online-boutique ns]
            Ashopfrontendvs([VirtualService]) -.-|host based routing to Service| Ashopfrontend[frontend]
        end
        subgraph Asimplens[simple-demo ns]
            Asimplefrontendvs([VirtualService]) -.-|host based routing to Service| Asimplefrontend[frontend]
        end
    end
    subgraph B[gke-central]
        subgraph Bistions[istio-system ns]
            Bgateway([Gateway]) -.- Bingress[istio ingressgateway]
        end
        subgraph Bbankns[bank-of-anthos ns]
            Bbankfrontendvs([VirtualService]) -.-|host based routing to Service| Bbankfrontend[frontend]
        end
        subgraph Bshopns[online-boutique ns]
            Bshopfrontendvs([VirtualService]) -.-|host based routing to Service| Bshopfrontend[frontend]
        end
        subgraph Bsimplens[simple-demo ns]
            Bsimplefrontendvs([VirtualService]) -.-|host based routing to Service| Bsimplefrontend[frontend]
        end
    end
end

subgraph C[gke-ingress]
    subgraph gkeingressistions[istio-system ns]
        multiclusteringress([MultiCusterIngress])
        multiclusterservice([MultiCusterService])
    end
end

ca[Cloud Armor] ==> GCLB ===> Aingress & Bingress
Aingress ==> Abankfrontendvs & Ashopfrontendvs & Asimplefrontendvs
Bingress ==> Bbankfrontendvs & Bshopfrontendvs & Bsimplefrontendvs
multiclusteringress -.->|configures MultiClusterIngress| GCLB
multiclusterservice -.->|configures MultiClusterService| Aingress & Bingress
end
Client ==> ca


classDef thickBox stroke-width:4px;
classDef noBox stroke-width:0px;
classDef dotted stroke-dasharray: 5 2;
class A,B,C thickBox;
class mesh,Aistions,Abankns,Ashopns,Asimplens,Bistions,Bbankns,Bshopns,Bsimplens,gkeingressistions dotted;
class GCP noBox;
```


## Objective

*   Create three GKE clusters. Two clusters are used to deploy applications while the third cluster, `ingress-config`, is used to configure MultiClusterIngress.
*   Register all cluster to an Environ so that you can utilize advanced features.
*   Enable MCI on the `ingress-config` cluster.
*   Install ASM on the two app clusters with custom ingress gateway.
*   Deploy three sample applications - [Bank of Anthos](https://github.com/GoogleCloudPlatform/bank-of-anthos), [Online Boutique](https://github.com/GoogleCloudPlatform/microservices-demo), and [Whereami](https://github.com/GoogleCloudPlatform/kubernetes-engine-samples/tree/master/whereami) on the two app clusters in three different namespaces.
*   Configure DNS names to access the `frontend` service of all the applications using Cloud Endpoints and create Google managed SSL certificates using the Cloud endpoints DNS names.
*   Configure a Cloud Armor security policy and rules.
*   Configure MCI to access Online Boutique, Bank of Anthos, and the Simple Demo applications through both clusters' `istio-ingressgateways` using a single MultiClusterIngress (GCLB frontend). 
*   Use Gateway and VirtualService resources on both clusters to send traffic to the `frontend` Services for all applications.

## Documentation

This tutorial uses the following documents:


*   [ASM Install](https://cloud.google.com/service-mesh/docs/scripted-install/gke-install)
*   [Multicluster Ingress](https://cloud.google.com/kubernetes-engine/docs/how-to/multi-cluster-ingress)
*   [From egde to mesh](https://cloud.google.com/solutions/exposing-service-mesh-apps-through-gke-ingress#mesh_ingress_gateway)
*   [Service Mesh Ingress](https://istio.io/latest/docs/tasks/traffic-management/ingress/ingress-control/)



## Environment



1. Create envars.

    ```bash
    # Enter your project ID below
    export PROJECT_ID=YOUR PROJECT ID HERE
    # Copy and paste the rest below
    gcloud config set project ${PROJECT_ID}
    export PROJECT_NUM=$(gcloud projects describe ${PROJECT_ID} --format='value(projectNumber)')
    export CLUSTER_1=gke-west
    export CLUSTER_2=gke-central
    export CLUSTER_1_ZONE=us-west2-a
    export CLUSTER_2_ZONE=us-central1-a
    export CLUSTER_INGRESS=gke-ingress
    export CLUSTER_INGRESS_ZONE=us-west1-a
    export WORKLOAD_POOL=${PROJECT_ID}.svc.id.goog
    export MESH_ID="proj-${PROJECT_NUM}"
    export ASM_VERSION=1.9
    export ISTIO_VERSION=1.9.5-asm.2
    export ASM_LABEL=asm-195-2
    export GSA_NAME=app-gsa
    export KSA_NAME=bank-ksa
    export BANK_DNS_PREFIX=bank
    export SHOP_DNS_PREFIX=shop
    export SIMPLE_DNS_PREFIX=simple
    ```



## Tooling



2. Create a WORKDIR folder.

    ```bash
    mkdir -p asm-mci && cd asm-mci && export WORKDIR=`pwd`
    ```



3. Install [krew](https://krew.sigs.k8s.io/).

    ```bash
    (
    set -x; cd "$(mktemp -d)" &&
    curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/krew.{tar.gz,yaml}" &&
    tar zxvf krew.tar.gz &&
    KREW=./krew-"$(uname | tr '[:upper:]' '[:lower:]')_amd64" &&
    $KREW install --manifest=krew.yaml --archive=krew.tar.gz &&
    $KREW update
    )
    echo -e "export PATH="${PATH}:${HOME}/.krew/bin"" >> ~/.bashrc && source ~/.bashrc
    ```



4. Install ctx and ns via krew for easy context switching.

    ```bash
    kubectl krew install ctx
    kubectl krew install ns
    ```



## Enable APIs



5. Enable the required APIs

    ```bash
    gcloud services enable \
    --project=${PROJECT_ID} \
    anthos.googleapis.com \
    container.googleapis.com \
    compute.googleapis.com \
    monitoring.googleapis.com \
    logging.googleapis.com \
    cloudtrace.googleapis.com \
    meshca.googleapis.com \
    meshtelemetry.googleapis.com \
    meshconfig.googleapis.com \
    iamcredentials.googleapis.com \
    gkeconnect.googleapis.com \
    gkehub.googleapis.com \
    multiclusteringress.googleapis.com \
    multiclusterservicediscovery.googleapis.com \
    cloudresourcemanager.googleapis.com
    ```



## Create GKE clusters



6. Create three GKE clusters. Two clusters are used for deploying applications while third cluster, the `ingress-config` cluster, is used to configure MulticlusterIngress.

    ```bash
    gcloud container clusters create ${CLUSTER_1} \
    --project ${PROJECT_ID} \
    --zone=${CLUSTER_1_ZONE} \
    --machine-type "e2-standard-4" \
    --num-nodes "2" --min-nodes "2" --max-nodes "5" \
    --enable-stackdriver-kubernetes --enable-ip-alias --enable-autoscaling \
    --workload-pool=${WORKLOAD_POOL} \
    --verbosity=none \
    --labels=mesh_id=${MESH_ID} --async

    gcloud container clusters create ${CLUSTER_2} \
    --project ${PROJECT_ID} \
    --zone=${CLUSTER_2_ZONE} \
    --machine-type "e2-standard-4" \
    --num-nodes "2" --min-nodes "2" --max-nodes "5" \
    --enable-stackdriver-kubernetes --enable-ip-alias --enable-autoscaling \
    --workload-pool=${WORKLOAD_POOL} \
    --verbosity=none \
    --labels=mesh_id=${MESH_ID} --async

    gcloud container clusters create ${CLUSTER_INGRESS} \
    --project ${PROJECT_ID} \
    --zone=${CLUSTER_INGRESS_ZONE} \
    --machine-type "e2-standard-4" \
    --num-nodes "1" --min-nodes "1" --max-nodes "2" \
    --enable-stackdriver-kubernetes --enable-ip-alias --enable-autoscaling \
    --verbosity=none \
    --workload-pool=${WORKLOAD_POOL}
    ```



7. Confirm clusters are `RUNNING`.

    ```bash
    gcloud container clusters list
    ```

The output is similar to the following:


    NAME         LOCATION       MASTER_VERSION    MASTER_IP       MACHINE_TYPE   NODE_VERSION      NUM_NODES  STATUS
    gke-central  us-central1-a  1.18.12-gke.1210  34.69.53.228    e2-standard-4  1.18.12-gke.1210  2          RUNNING
    gke-ingress  us-west1-a     1.18.12-gke.1210  35.230.36.119   e2-standard-4  1.18.12-gke.1210  1          RUNNING
    gke-west     us-west2-a     1.18.15-gke.1501  35.235.122.105  e2-standard-4  1.18.15-gke.1501  2          RUNNING


8. Connect to clusters.

    ```bash
    touch ${WORKDIR}/asm-kubeconfig && export KUBECONFIG=${WORKDIR}/asm-kubeconfig
    gcloud container clusters get-credentials ${CLUSTER_1} --zone ${CLUSTER_1_ZONE}
    gcloud container clusters get-credentials ${CLUSTER_2} --zone ${CLUSTER_2_ZONE}
    gcloud container clusters get-credentials ${CLUSTER_INGRESS} --zone ${CLUSTER_INGRESS_ZONE}

    ```

> Remember to unset your `KUBECONFIG` var at the end.

9. Rename cluster context for easy switching.

    ```bash
    kubectl ctx ${CLUSTER_1}=gke_${PROJECT_ID}_${CLUSTER_1_ZONE}_${CLUSTER_1}
    kubectl ctx ${CLUSTER_2}=gke_${PROJECT_ID}_${CLUSTER_2_ZONE}_${CLUSTER_2}
    kubectl ctx ${CLUSTER_INGRESS}=gke_${PROJECT_ID}_${CLUSTER_INGRESS_ZONE}_${CLUSTER_INGRESS}
    ```



10. Confirm both cluster contextx are present.

    ```bash
    kubectl ctx
    ```

The output is similar to the following: 

    gke-central
    gke-ingress
    gke-west




## Registering clusters to an Environ



11. Register all clusters to an [Environ](https://cloud.google.com/anthos/multicluster-management/environs).

    ```bash
    gcloud container hub memberships register ${CLUSTER_INGRESS} \
    --project=${PROJECT_ID} \
    --gke-cluster=${CLUSTER_INGRESS_ZONE}/${CLUSTER_INGRESS} \
    --enable-workload-identity

    gcloud container hub memberships register ${CLUSTER_1} \
    --project=${PROJECT_ID} \
    --gke-cluster=${CLUSTER_1_ZONE}/${CLUSTER_1} \
    --enable-workload-identity

    gcloud container hub memberships register ${CLUSTER_2} \
    --project=${PROJECT_ID} \
    --gke-cluster=${CLUSTER_2_ZONE}/${CLUSTER_2} \
    --enable-workload-identity

    gcloud container hub memberships list
    ```

The output is similar to the following: 


    NAME            EXTERNAL_ID
    gke-west        7fe5b7ce-50d0-4e64-a9af-55d37b3dd3fa
    gke-central     6f1f6bb2-a3f6-4e9c-be52-6907d9d258cd
    gke-ingress     3574ee0f-b7e6-11ea-9787-42010a8a019c


12. Enable MCI in the CLUSTER_INGRESS

    ```bash
    gcloud alpha container hub ingress enable \
    --config-membership=projects/${PROJECT_ID}/locations/global/memberships/${CLUSTER_INGRESS}

    gcloud alpha container hub ingress describe
    ```

The output is similar to the following:


    featureState:
    details:
        code: OK
        description: Ready to use
    detailsByMembership:
        projects/217631055577/locations/global/memberships/gke-central:
        code: OK
        projects/217631055577/locations/global/memberships/gke-ingress:
        code: OK
        projects/217631055577/locations/global/memberships/gke-west:
        code: OK
    lifecycleState: ENABLED
    multiclusteringressFeatureSpec:
    configMembership: projects/asm-multi-environment-cluster/locations/global/memberships/gke-ingress
    name: projects/asm-multi-environment-cluster/locations/global/features/multiclusteringress

> It takes a few moments for the controller to configure MCI. You may have to run the describe command a few times. Do not proceed until you see the output shown.

13. Verify that the two CRDs (MCS and MCI) have been deployed in the CLUSTER_INGRESS.

    ```bash
    kubectl --context=${CLUSTER_INGRESS} get crd | grep multicluster
    ```

The output is similar to the following:


    multiclusteringresses.networking.gke.io     2020-10-29T17:32:50Z
    multiclusterservices.networking.gke.io      2020-10-29T17:32:50Z




14. Create FW rule to allow traffic between Pods in all clusters.

    ```bash
    gcloud compute firewall-rules create all-10 \
    --project ${PROJECT_ID} \
    --network default \
    --allow all \
    --direction INGRESS \
    --source-ranges 10.0.0.0/8
    ```



## Installing ASM



15. Download the version of the script that installs Anthos Service Mesh to the current working directory.

    ```bash
    curl https://storage.googleapis.com/csm-artifacts/asm/install_asm_"${ASM_VERSION}" > install_asm_19
    chmod +x install_asm_19
    ```



16. Create `outdir` folders for the wo clusters. The output of the following scripts are stored in these folders. You also need the version specific `istioctl` CLI utiltiy for later steps.

    ```bash
    mkdir -p ${WORKDIR}/asm-${CLUSTER_1} && mkdir -p ${WORKDIR}/asm-${CLUSTER_1}
    ```



17. Create ingress gateway customization.

    ```bash
    cat <<EOF > ${WORKDIR}/custom-ingressgateway.yaml
    apiVersion: install.istio.io/v1alpha1
    kind: IstioOperator
    spec:
      components:
        ingressGateways:
        - name: istio-ingressgateway
          enabled: true
          k8s:
            hpaSpec:
              maxReplicas: 10
              minReplicas: 2
            service:
              type: ClusterIP
    EOF
    ```



18. Install ASM on both clusters using the `install_asm.sh` script.

    ```bash
    ./install_asm_19 \
    --project_id ${PROJECT_ID} \
    --cluster_name ${CLUSTER_1} \
    --cluster_location ${CLUSTER_1_ZONE} \
    --mode install \
    --option envoy-access-log \
    --custom_overlay ${WORKDIR}/custom-ingressgateway.yaml \
    --output_dir ${WORKDIR}/asm-${CLUSTER_1} \
    --enable_all

    ./install_asm_19 \
    --project_id ${PROJECT_ID} \
    --cluster_name ${CLUSTER_2} \
    --cluster_location ${CLUSTER_2_ZONE} \
    --mode install \
    --option envoy-access-log \
    --custom_overlay ${WORKDIR}/custom-ingressgateway.yaml \
    --output_dir ${WORKDIR}/asm-${CLUSTER_2} \
    --enable_all
    ```


The output is similar to the following:


    ...
    install_asm: Successfully installed ASM.




19. Validate ingress gateway customizations.

    ```bash
    kubectl --context=${CLUSTER_1} -n istio-system get svc istio-ingressgateway
    kubectl --context=${CLUSTER_1} -n istio-system get hpa istio-ingressgateway
    kubectl --context=${CLUSTER_2} -n istio-system get svc istio-ingressgateway
    kubectl --context=${CLUSTER_2} -n istio-system get hpa istio-ingressgateway
    ```


The output is similar to the following: 


    # Service does not have an EXTERNAL-IP
    NAME                   TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)                                        AGE
    istio-ingressgateway   ClusterIP   10.92.12.33   <none>        15021/TCP,80/TCP,443/TCP,15012/TCP,15443/TCP   2m9s

    # HPA MAXPODS has changed from 5 to 10
    NAME                   REFERENCE                         TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
    istio-ingressgateway   Deployment/istio-ingressgateway   3%/80%    2         10        2          2m9s



The `EXTERNAL_IP` on the `istio-ingressgateway` Service should be none and the HPA should be updated to the customized value (in this case, the maxReplicas was changed from 5 to 10).


20. Get `istioctl` CLI.

    ```bash
    export ISTIOCTL_CMD=${WORKDIR}/asm-${CLUSTER_1}/istio-${ISTIO_VERSION}/bin/istioctl
    ${ISTIOCTL_CMD} version
    ```

The output is similar to the following: 


    client version: 1.9.1-asm.1
    control plane version: 1.9.1-asm.1
    data plane version: 1.9.1-asm.1 (2 proxies)




### Cross cluster service discovery



21. Create a secret with the CLUSTER_1 kubeconfig and deploy to CLUSTER_2.

    ```bash
    ${ISTIOCTL_CMD} x create-remote-secret \
    --context=${CLUSTER_1} \
    --name=${CLUSTER_1} > secret-kubeconfig-${CLUSTER_1}.yaml

    kubectl --context=${CLUSTER_2} -n istio-system apply -f secret-kubeconfig-${CLUSTER_1}.yaml
    ```



22. Create a secret with the CLUSTER_2 kubeconfig and deploy to CLUSTER_1.

    ```bash
    ${ISTIOCTL_CMD} x create-remote-secret \
    --context=${CLUSTER_2} \
    --name=${CLUSTER_2} > secret-kubeconfig-${CLUSTER_2}.yaml

    kubectl --context=${CLUSTER_1} -n istio-system apply -f secret-kubeconfig-${CLUSTER_2}.yaml
    ```



## Bank of Anthos

In this section, you deploy Bank of Anthos to CLUSTER_1 and CLUSTER_2.



23. Clone the Bank of Anthos repo.

    ```bash
    git clone https://github.com/GoogleCloudPlatform/bank-of-anthos.git bank-of-anthos
    ```



24. Create and label `bank-of-anthos` namespace in both cluster. The ASM label ensures that the Envoy proxies get automatically injected into each Pod in that namespace.

    ```bash
    kubectl create --context=${CLUSTER_1} namespace bank-of-anthos
    kubectl label --context=${CLUSTER_1} namespace bank-of-anthos istio.io/rev=${ASM_LABEL}
    kubectl create --context=${CLUSTER_2} namespace bank-of-anthos
    kubectl label --context=${CLUSTER_2} namespace bank-of-anthos istio.io/rev=${ASM_LABEL}
    ```



25. Create a Google Service Account to set it up with Workload Identity. With Workload Identity, Kubernetes Service Accounts can authenticate and access Google APIs. Bank of Anthos requires access to Cloud Ops for metrics and traces.

    ```bash
    gcloud iam service-accounts create ${GSA_NAME}
    kubectl --context=${CLUSTER_1} create serviceaccount --namespace bank-of-anthos ${KSA_NAME}
    kubectl --context=${CLUSTER_2} create serviceaccount --namespace bank-of-anthos ${KSA_NAME}
    ```



26. Create IAM roles.

    ```bash
    gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member "serviceAccount:${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --role roles/cloudtrace.agent

    gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member "serviceAccount:${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --role roles/monitoring.metricWriter

    gcloud iam service-accounts add-iam-policy-binding \
    --role roles/iam.workloadIdentityUser \
    --member "serviceAccount:${PROJECT_ID}.svc.id.goog[bank-of-anthos/${KSA_NAME}]" \
    ${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com

    kubectl --context=$CLUSTER_1 annotate serviceaccount \
    --namespace bank-of-anthos \
    ${KSA_NAME} \
    iam.gke.io/gcp-service-account=${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com

    kubectl --context=$CLUSTER_2 annotate serviceaccount \
    --namespace bank-of-anthos \
    ${KSA_NAME} \
    iam.gke.io/gcp-service-account=${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com

    mkdir -p ${WORKDIR}/wi-kubernetes-manifests
    FILES="${WORKDIR}/bank-of-anthos/kubernetes-manifests/*"
    for f in $FILES; do
        echo "Processing $f..."
        sed -e "s/serviceAccountName: default/serviceAccountName: ${KSA_NAME}/g" -e "s/0.4.3/0.4.2/g" $f > ${WORKDIR}/wi-kubernetes-manifests/`basename $f`
    done
    ```



27. Deploy Bank of Anthos application to both cluisters in the `bank-of-anthos` namespace. Deploy the two PostGres DBs only in one of two clusters by deleting the StatefulSets from the other cluster.

    ```bash
    kubectl --context=$CLUSTER_1 -n bank-of-anthos apply -f bank-of-anthos/extras/jwt/jwt-secret.yaml
    kubectl --context=$CLUSTER_2 -n bank-of-anthos apply -f bank-of-anthos/extras/jwt/jwt-secret.yaml
    kubectl --context=$CLUSTER_1 -n bank-of-anthos apply -f ./wi-kubernetes-manifests
    kubectl --context=$CLUSTER_2 -n bank-of-anthos apply -f ./wi-kubernetes-manifests
    kubectl --context=$CLUSTER_2 -n bank-of-anthos delete statefulset accounts-db
    kubectl --context=$CLUSTER_2 -n bank-of-anthos delete statefulset ledger-db
    ```


All Services are deployed as Distributed Services. This means that there are Deployments of all Services on both clusters. The `accounts-db` and `ledger-db` is only deployed to CLUSTER1.


28. Verify that all Pods are running in both clusters.

    ```bash
    kubectl --context=${CLUSTER_1} -n bank-of-anthos wait --for=condition=ready --timeout=5m pod accounts-db-0
    kubectl --context=${CLUSTER_1} -n bank-of-anthos wait --for=condition=ready --timeout=5m pod ledger-db-0
    kubectl --context=${CLUSTER_1} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment contacts
    kubectl --context=${CLUSTER_1} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment frontend
    kubectl --context=${CLUSTER_1} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment ledgerwriter
    kubectl --context=${CLUSTER_1} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment loadgenerator
    kubectl --context=${CLUSTER_1} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment balancereader
    kubectl --context=${CLUSTER_1} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment transactionhistory
    kubectl --context=${CLUSTER_1} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment userservice

    kubectl --context=${CLUSTER_2} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment contacts
    kubectl --context=${CLUSTER_2} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment frontend
    kubectl --context=${CLUSTER_2} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment ledgerwriter
    kubectl --context=${CLUSTER_2} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment loadgenerator
    kubectl --context=${CLUSTER_2} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment balancereader
    kubectl --context=${CLUSTER_2} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment transactionhistory
    kubectl --context=${CLUSTER_2} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment userservice
    ```

The output is similar to the following: 


    deployment.apps/frontend condition met


29. Get Pods from both clusters.

    ```bash
    kubectl --context=${CLUSTER_1} -n bank-of-anthos get pod
    kubectl --context=${CLUSTER_2} -n bank-of-anthos get pod
    ```

The output is similar to the following: 



    # CLUSTER1
    NAME                                  READY   STATUS    RESTARTS   AGE
    accounts-db-0                         2/2     Running   0          2m40s
    balancereader-c5d664b4c-tf8fg         2/2     Running   0          2m40s
    contacts-7fd8c5fb6-vprp9              2/2     Running   1          2m39s
    frontend-7b7fb9b665-g9wzd             2/2     Running   1          2m39s
    ledger-db-0                           2/2     Running   0          2m38s
    ledgerwriter-7b5b6db66f-2hsqt         2/2     Running   0          2m38s
    loadgenerator-7fb54d57f8-85bdc        2/2     Running   0          2m37s
    transactionhistory-7fdb998c5f-ns29r   2/2     Running   0          2m37s
    userservice-76996974f5-tv56x          2/2     Running   0          2m37s

    # CLUSTER2
    NAME                                  READY   STATUS    RESTARTS   AGE
    balancereader-c5d664b4c-vwjrw         2/2     Running   1          2m37s
    contacts-7fd8c5fb6-5c2l9              2/2     Running   1          2m36s
    frontend-7b7fb9b665-gjllh             2/2     Running   1          2m36s
    ledgerwriter-7b5b6db66f-lp2vn         2/2     Running   0          2m35s
    loadgenerator-7fb54d57f8-wpsct        2/2     Running   0          2m34s
    transactionhistory-7fdb998c5f-8ndqx   2/2     Running   0          2m34s
    userservice-76996974f5-n6xwp          2/2     Running   1          2m34s


30. Deploy the ASM configs to both clusters.

    ```bash
    kubectl --context=${CLUSTER_1} -n bank-of-anthos apply -f bank-of-anthos/istio-manifests
    kubectl --context=${CLUSTER_2} -n bank-of-anthos apply -f bank-of-anthos/istio-manifests
    ```



31. Get the frontend Pod from CLUSTER1.

    ```bash
    export BANK_CLUSTER_1_FRONTEND_POD=$(kubectl get pod -n bank-of-anthos -l app=frontend --context=${CLUSTER_1} -o jsonpath='{.items[0].metadata.name}')
    ```


32. Inspect the proxy-config endpoints on the frontend Pod.

    ```bash
    ${ISTIOCTL_CMD} --context $CLUSTER_1 -n bank-of-anthos pc ep $BANK_CLUSTER_1_FRONTEND_POD | grep bank-of-anthos
    ```

The output is similar to the following:


    10.0.0.10:8080                   HEALTHY     OK                outbound|80||frontend.bank-of-anthos.svc.cluster.local
    10.0.0.11:8080                   HEALTHY     OK                outbound|8080||userservice.bank-of-anthos.svc.cluster.local
    10.0.1.6:5432                    HEALTHY     OK                outbound|5432||accounts-db.bank-of-anthos.svc.cluster.local
    10.0.1.7:8080                    HEALTHY     OK                outbound|8080||contacts.bank-of-anthos.svc.cluster.local
    10.0.2.10:8080                   HEALTHY     OK                outbound|8080||ledgerwriter.bank-of-anthos.svc.cluster.local
    10.0.2.12:8080                   HEALTHY     OK                outbound|8080||transactionhistory.bank-of-anthos.svc.cluster.local
    10.0.2.8:8080                    HEALTHY     OK                outbound|8080||balancereader.bank-of-anthos.svc.cluster.local
    10.0.2.9:5432                    HEALTHY     OK                outbound|5432||ledger-db.bank-of-anthos.svc.cluster.local
    10.0.3.7:8080                    HEALTHY     OK                outbound|8080||balancereader.bank-of-anthos.svc.cluster.local
    10.0.3.8:8080                    HEALTHY     OK                outbound|8080||contacts.bank-of-anthos.svc.cluster.local
    10.0.3.9:8080                    HEALTHY     OK                outbound|8080||userservice.bank-of-anthos.svc.cluster.local
    10.0.4.11:8080                   HEALTHY     OK                outbound|8080||ledgerwriter.bank-of-anthos.svc.cluster.local
    10.0.4.12:8080                   HEALTHY     OK                outbound|8080||transactionhistory.bank-of-anthos.svc.cluster.local
    10.0.5.8:8080                    HEALTHY     OK                outbound|80||frontend.bank-of-anthos.svc.cluster.local


With the exception of `accounts-db` and `ledger-db`, all other Services have two Endpoints. One Pod in each cluster. 




## Online Boutique

In this section, you deploy Online Boutique to CLUSTER1 and CLUSTER2.


33. Clone the Online Boutique repo.

    ```bash
    git clone https://github.com/GoogleCloudPlatform/microservices-demo online-boutique
    ```



34. Create and label `online-boutique` namespace in both cluster. The ASM label ensures that the Envoy proxies get automatically injected into each Pod in that namespace.

    ```bash
    kubectl create --context=${CLUSTER_1} namespace online-boutique
    kubectl label --context=${CLUSTER_1} namespace online-boutique istio.io/rev=${ASM_LABEL}
    kubectl create --context=${CLUSTER_2} namespace online-boutique
    kubectl label --context=${CLUSTER_2} namespace online-boutique istio.io/rev=${ASM_LABEL}
    ```



35. Deploy Online Boutique to both cluisters in the `online-boutique` namespace. Deploy redis-cart Deployment only in one of two clusters. Redis is used by cartservice to store items in your shopping cart. It is the only stateful service in Online Boutique.

    ```bash
    kubectl --context=$CLUSTER_1 -n online-boutique apply -f online-boutique/release
    kubectl --context=$CLUSTER_2 -n online-boutique apply -f online-boutique/release
    kubectl --context=$CLUSTER_2 -n online-boutique delete deployment redis-cart
    ```


All Services are deployed as Distributed Services. This means that there are Deployments of all Services on both clusters. The `redis-cart` is only deployed to CLUSTER_1.


36. Verify that all Pods are running in both clusters.

    ```bash
    kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment adservice
    kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment cartservice
    kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment checkoutservice
    kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment currencyservice
    kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment emailservice
    kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment frontend
    kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment loadgenerator
    kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment paymentservice
    kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment productcatalogservice
    kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment recommendationservice
    kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment redis-cart
    kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment shippingservice

    kubectl --context=${CLUSTER_2} -n online-boutique wait --for=condition=available --timeout=5m deployment adservice
    kubectl --context=${CLUSTER_2} -n online-boutique wait --for=condition=available --timeout=5m deployment cartservice
    kubectl --context=${CLUSTER_2} -n online-boutique wait --for=condition=available --timeout=5m deployment checkoutservice
    kubectl --context=${CLUSTER_2} -n online-boutique wait --for=condition=available --timeout=5m deployment currencyservice
    kubectl --context=${CLUSTER_2} -n online-boutique wait --for=condition=available --timeout=5m deployment emailservice
    kubectl --context=${CLUSTER_2} -n online-boutique wait --for=condition=available --timeout=5m deployment frontend
    kubectl --context=${CLUSTER_2} -n online-boutique wait --for=condition=available --timeout=5m deployment loadgenerator
    kubectl --context=${CLUSTER_2} -n online-boutique wait --for=condition=available --timeout=5m deployment paymentservice
    kubectl --context=${CLUSTER_2} -n online-boutique wait --for=condition=available --timeout=5m deployment productcatalogservice
    kubectl --context=${CLUSTER_2} -n online-boutique wait --for=condition=available --timeout=5m deployment recommendationservice
    kubectl --context=${CLUSTER_2} -n online-boutique wait --for=condition=available --timeout=5m deployment shippingservice
    ```

The output is similar to the following: 


    deployment.apps/frontend condition met


37. Get Pods from both clusters.

    ```bash
    kubectl --context=${CLUSTER_1} -n online-boutique get pod
    kubectl --context=${CLUSTER_2} -n online-boutique get pod
    ```

The output is similar to the following:


    # CLUSTER_1
    NAME                                     READY   STATUS    RESTARTS   AGE
    adservice-76bdd69666-zcsjk               2/2     Running   0          3m12s
    cartservice-66d497c6b7-5vgr2             2/2     Running   1          3m14s
    checkoutservice-666c784bd6-tsx9k         2/2     Running   0          3m15s
    currencyservice-5d5d496984-ghfs5         2/2     Running   0          3m13s
    emailservice-667457d9d6-7zn6m            2/2     Running   0          3m15s
    frontend-6b8d69b9fb-mgx9w                2/2     Running   0          3m15s
    loadgenerator-665b5cd444-l2nrf           2/2     Running   2          3m13s
    paymentservice-68596d6dd6-b47lq          2/2     Running   0          3m14s
    productcatalogservice-557d474574-bs6p5   2/2     Running   0          3m14s
    recommendationservice-69c56b74d4-9zk7l   2/2     Running   0          3m15s
    redis-cart-5f59546cdd-bgpvh              2/2     Running   0          3m12s
    shippingservice-6ccc89f8fd-v6q7l         2/2     Running   0          3m13s

    # CLUSTER_2
    NAME                                     READY   STATUS    RESTARTS   AGE
    adservice-76bdd69666-s7t79               2/2     Running   0          3m6s
    cartservice-66d497c6b7-5lpwp             2/2     Running   1          3m7s
    checkoutservice-666c784bd6-9vwb4         2/2     Running   0          3m9s
    currencyservice-5d5d496984-62fbr         2/2     Running   0          3m7s
    emailservice-667457d9d6-t95b5            2/2     Running   0          3m9s
    frontend-6b8d69b9fb-xppk2                2/2     Running   0          3m9s
    loadgenerator-665b5cd444-98hpm           2/2     Running   2          3m7s
    paymentservice-68596d6dd6-f2jvp          2/2     Running   0          3m8s
    productcatalogservice-557d474574-wqndp   2/2     Running   0          3m8s
    recommendationservice-69c56b74d4-vwt79   2/2     Running   0          3m9s
    shippingservice-6ccc89f8fd-rlhcc         2/2     Running   0          3m7


38. Get the frontend Pod from CLUSTER1.

    ```bash
    export SHOP_CLUSTER1_FRONTEND_POD=$(kubectl get pod -n online-boutique -l app=frontend --context=${CLUSTER_1} -o jsonpath='{.items[0].metadata.name}')
    ```


39. Inspect the proxy-config endpoints on the frontend Pod.

    ```bash
    ${ISTIOCTL_CMD} --context $CLUSTER_1 -n online-boutique pc ep $SHOP_CLUSTER1_FRONTEND_POD | grep online-boutique
    ```


The output is similar to the following:



    10.8.0.7:50051                   HEALTHY     OK                outbound|50051||paymentservice.online-boutique.svc.cluster.local
    10.8.0.8:7070                    HEALTHY     OK                outbound|7070||cartservice.online-boutique.svc.cluster.local
    10.8.0.9:6379                    HEALTHY     OK                outbound|6379||redis-cart.online-boutique.svc.cluster.local
    10.8.1.10:8080                   HEALTHY     OK                outbound|8080||recommendationservice.online-boutique.svc.cluster.local
    10.8.1.11:5050                   HEALTHY     OK                outbound|5050||checkoutservice.online-boutique.svc.cluster.local
    10.8.1.13:7000                   HEALTHY     OK                outbound|7000||currencyservice.online-boutique.svc.cluster.local
    10.8.2.10:50051                  HEALTHY     OK                outbound|50051||shippingservice.online-boutique.svc.cluster.local
    10.8.2.11:9555                   HEALTHY     OK                outbound|9555||adservice.online-boutique.svc.cluster.local
    10.8.2.7:8080                    HEALTHY     OK                outbound|80||frontend-external.online-boutique.svc.cluster.local
    10.8.2.7:8080                    HEALTHY     OK                outbound|80||frontend.online-boutique.svc.cluster.local
    10.8.2.8:8080                    HEALTHY     OK                outbound|5000||emailservice.online-boutique.svc.cluster.local
    10.8.2.9:3550                    HEALTHY     OK                outbound|3550||productcatalogservice.online-boutique.svc.cluster.local
    10.8.3.5:8080                    HEALTHY     OK                outbound|8080||recommendationservice.online-boutique.svc.cluster.local
    10.8.3.6:50051                   HEALTHY     OK                outbound|50051||paymentservice.online-boutique.svc.cluster.local
    10.8.3.7:5050                    HEALTHY     OK                outbound|5050||checkoutservice.online-boutique.svc.cluster.local
    10.8.3.8:7070                    HEALTHY     OK                outbound|7070||cartservice.online-boutique.svc.cluster.local
    10.8.3.9:7000                    HEALTHY     OK                outbound|7000||currencyservice.online-boutique.svc.cluster.local
    10.8.4.9:50051                   HEALTHY     OK                outbound|50051||shippingservice.online-boutique.svc.cluster.local
    10.8.5.10:8080                   HEALTHY     OK                outbound|80||frontend-external.online-boutique.svc.cluster.local
    10.8.5.10:8080                   HEALTHY     OK                outbound|80||frontend.online-boutique.svc.cluster.local
    10.8.5.11:8080                   HEALTHY     OK                outbound|5000||emailservice.online-boutique.svc.cluster.local
    10.8.5.13:9555                   HEALTHY     OK                outbound|9555||adservice.online-boutique.svc.cluster.local
    10.8.5.9:3550                    HEALTHY     OK                outbound|3550||productcatalogservice.online-boutique.svc.cluster.local


With the exception of `redis-cart`, all other Services have two Endpoints. One Pod in each cluster. 


## Simple Demo


40. Create and label `simple-demo` namespace in both cluster. The ASM label ensures that the Envoy proxies get automatically injected into each Pod in that namespace.

    ```bash
    kubectl create --context=${CLUSTER_1} namespace simple-demo
    kubectl label --context=${CLUSTER_1} namespace simple-demo istio.io/rev=${ASM_LABEL}
    kubectl create --context=${CLUSTER_2} namespace simple-demo
    kubectl label --context=${CLUSTER_2} namespace simple-demo istio.io/rev=${ASM_LABEL}
    ```

41. Deploy whereami app to both cluisters in the `simple-demo` namespace.

    ```bash
    git clone https://github.com/theemadnes/gke-whereami.git simple-demo && cd simple-demo
    kubectl apply -k k8s-backend-overlay-example/ --context=${CLUSTER_1} -n simple-demo
    kubectl apply -k k8s-backend-overlay-example/ --context=${CLUSTER_2} -n simple-demo
    kubectl apply -k k8s-frontend-overlay-example/ --context=${CLUSTER_1} -n simple-demo
    kubectl apply -k k8s-frontend-overlay-example/ --context=${CLUSTER_2} -n simple-demo
    ```

42. Verify that all Pods are running in both clusters.

    ```bash
    kubectl --context=${CLUSTER_1} -n simple-demo wait --for=condition=available --timeout=5m deployment whereami-frontend
    kubectl --context=${CLUSTER_1} -n simple-demo wait --for=condition=available --timeout=5m deployment whereami-backend
    kubectl --context=${CLUSTER_2} -n simple-demo wait --for=condition=available --timeout=5m deployment whereami-frontend
    kubectl --context=${CLUSTER_2} -n simple-demo wait --for=condition=available --timeout=5m deployment whereami-backend
    ```

    The output is similar to the following: 

        deployment.apps/whereami-frontend condition met


43. Get Pods from both clusters.

    ```bash
    kubectl --context=${CLUSTER_1} -n simple-demo get pod
    kubectl --context=${CLUSTER_2} -n simple-demo get pod
    ```
The output is similar to the following:

    # CLUSTER_1
    NAME                                 READY   STATUS    RESTARTS   AGE
    whereami-backend-86b544f56-j5gbd     2/2     Running   0          37h
    whereami-backend-86b544f56-jgbjj     2/2     Running   0          37h
    whereami-backend-86b544f56-zhtkk     2/2     Running   0          37h
    whereami-frontend-6fd4c4cd58-fd42q   2/2     Running   0          37h
    whereami-frontend-6fd4c4cd58-jcszb   2/2     Running   0          37h
    whereami-frontend-6fd4c4cd58-vf5fd   2/2     Running   0          37h

    # CLUSTER_2
    NAME                                 READY   STATUS    RESTARTS   AGE
    whereami-backend-86b544f56-9b6nm     2/2     Running   0          39h
    whereami-backend-86b544f56-jdswq     2/2     Running   0          39h
    whereami-backend-86b544f56-tmv6h     2/2     Running   0          39h
    whereami-frontend-6fd4c4cd58-kpz8d   2/2     Running   0          39h
    whereami-frontend-6fd4c4cd58-n7bhl   2/2     Running   0          39h
    whereami-frontend-6fd4c4cd58-v257f   2/2     Running   0          39h


44. Get the frontend Pod from CLUSTER1.

    ```bash
    export SIMPLE_CLUSTER1_FRONTEND_POD=$(kubectl get pod -n simple-demo -l app=whereami-frontend --context=${CLUSTER_1} -o jsonpath='{.items[0].metadata.name}')


The output is similar to the following:

    10.60.2.10:8080                  HEALTHY     OK                outbound|80||whereami-backend.simple-demo.svc.cluster.local
    10.60.2.11:8080                  HEALTHY     OK                outbound|80||whereami-frontend.simple-demo.svc.cluster.local
    10.60.2.12:8080                  HEALTHY     OK                outbound|80||whereami-frontend.simple-demo.svc.cluster.local
    10.60.2.13:8080                  HEALTHY     OK                outbound|80||whereami-frontend.simple-demo.svc.cluster.local
    10.60.2.8:8080                   HEALTHY     OK                outbound|80||whereami-backend.simple-demo.svc.cluster.local
    10.60.2.9:8080                   HEALTHY     OK                outbound|80||whereami-backend.simple-demo.svc.cluster.local
    10.88.0.26:8080                  HEALTHY     OK                outbound|80||whereami-backend.simple-demo.svc.cluster.local
    10.88.0.27:8080                  HEALTHY     OK                outbound|80||whereami-backend.simple-demo.svc.cluster.local
    10.88.0.28:8080                  HEALTHY     OK                outbound|80||whereami-frontend.simple-demo.svc.cluster.local
    10.88.1.26:8080                  HEALTHY     OK                outbound|80||whereami-backend.simple-demo.svc.cluster.local
    10.88.1.27:8080                  HEALTHY     OK                outbound|80||whereami-frontend.simple-demo.svc.cluster.local
    10.88.1.28:8080                  HEALTHY     OK                outbound|80||whereami-frontend.simple-demo.svc.cluster.local


45. Inspect the proxy-config endpoints on the frontend Pod.

    ```bash
    ${ISTIOCTL_CMD} --context $CLUSTER_1 -n simple-demo pc ep $SIMPLE_CLUSTER1_FRONTEND_POD | grep simple
    ```

## Configure DNS and Google managed certificates



46. Create a global static IP for GCLB.

    ```bash
    gcloud compute addresses create gclb-ip --global
    ```



47. Get the static IP.

    ```bash
    export GCLB_IP=$(gcloud compute addresses describe gclb-ip --global --format=json | jq -r '.address')
    ```



48. Create a free DNS name in the `cloud.goog` domain using Cloud Endpoints DNS service for Bank of Anthos.

    ```bash
    cat <<EOF > ${WORKDIR}/bank-openapi.yaml
    swagger: "2.0"
    info:
      description: "Cloud Endpoints DNS"
      title: "Cloud Endpoints DNS"
      version: "1.0.0"
    paths: {}
    host: "${BANK_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    x-google-endpoints:
    - name: "${BANK_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
      target: "${GCLB_IP}"
    EOF
    ```



49. Create a free DNS name in the `cloud.goog` domain using Cloud Endpoints DNS service for Online Boutique.

    ```bash
    cat <<EOF > ${WORKDIR}/shop-openapi.yaml
    swagger: "2.0"
    info:
      description: "Cloud Endpoints DNS"
      title: "Cloud Endpoints DNS"
      version: "1.0.0"
    paths: {}
    host: "${SHOP_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    x-google-endpoints:
    - name: "${SHOP_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
      target: "${GCLB_IP}"
    EOF
    ```


50. Create a free DNS name in the `cloud.goog` domain using Cloud Endpoints DNS service for Simple Demo.

    ```bash
    cat <<EOF > ${WORKDIR}/simple-openapi.yaml
    swagger: "2.0"
    info:
      description: "Cloud Endpoints DNS"
      title: "Cloud Endpoints DNS"
      version: "1.0.0"
    paths: {}
    host: "${SIMPLE_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    x-google-endpoints:
    - name: "${SIMPLE_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
      target: "${GCLB_IP}"
    EOF
    ```



51. Deploy the openapi.yaml files.

    ```bash
    gcloud endpoints services deploy ${WORKDIR}/bank-openapi.yaml
    gcloud endpoints services deploy ${WORKDIR}/shop-openapi.yaml
    gcloud endpoints services deploy ${WORKDIR}/simple-openapi.yaml
    ```



52. Create a Google managed cert for the domains.

    ```bash
    cat <<EOF > bank-managed-cert.yaml
    apiVersion: networking.gke.io/v1beta2
    kind: ManagedCertificate
    metadata:
      name: bank-managed-cert
      namespace: istio-system
    spec:
      domains:
      - "${BANK_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    EOF

    cat <<EOF > shop-managed-cert.yaml
    apiVersion: networking.gke.io/v1beta2
    kind: ManagedCertificate
    metadata:
      name: shop-managed-cert
      namespace: istio-system
    spec:
      domains:
      - "${SHOP_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    EOF

    cat <<EOF > simple-managed-cert.yaml
    apiVersion: networking.gke.io/v1beta2
    kind: ManagedCertificate
    metadata:
      name: simple-managed-cert
      namespace: istio-system
    spec:
      domains:
      - "${SIMPLE_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    EOF

    kubectl --context ${CLUSTER_INGRESS} create namespace istio-system
    kubectl --context ${CLUSTER_INGRESS} apply -f bank-managed-cert.yaml
    kubectl --context ${CLUSTER_INGRESS} apply -f shop-managed-cert.yaml
    kubectl --context ${CLUSTER_INGRESS} apply -f simple-managed-cert.yaml
    ```



53. You can view the status of your certificates by descriving these resources. The `spec` and `status` fields show configured data.

    ```bash
    kubectl --context ${CLUSTER_INGRESS} -n istio-system describe managedcertificate bank-managed-cert
    ```


The output is similar to the following:



    Spec:
    Domains:
        boa1.endpoints.asm-multi-environment-cluster.cloud.goog
    Status:
    Certificate Name:    mcrt-f90cd39f-c706-4e51-9835-8493833ec2f8
    Certificate Status:  Provisioning
    Domain Status:
        Domain:  boa1.endpoints.asm-multi-environment-cluster.cloud.goog
        Status:  Provisioning


54. Get the certificate resource names.

    ```bash
    export BANK_MANAGED_CERT=$(kubectl --context ${CLUSTER_INGRESS} -n istio-system get managedcertificate bank-managed-cert -ojsonpath='{.status.certificateName}')
    export SHOP_MANAGED_CERT=$(kubectl --context ${CLUSTER_INGRESS} -n istio-system get managedcertificate shop-managed-cert -ojsonpath='{.status.certificateName}')
    export SIMPLE_MANAGED_CERT=$(kubectl --context ${CLUSTER_INGRESS} -n istio-system get managedcertificate simple-managed-cert -ojsonpath='{.status.certificateName}')
    ```


> It can take up to 30 minutes for the certificate to provision. You can proceed with the following steps.





55. Create Cloud Armor policies. [Google Cloud Armor](https://cloud.google.com/armor) provides DDoS defense and [customizable security policies](https://cloud.google.com/armor/docs/configure-security-policies) that you can attach to a load balancer through Ingress resources. In the following steps, you create a security policy that uses [preconfigured rules](https://cloud.google.com/armor/docs/rule-tuning#preconfigured_rules) to block cross-site scripting (XSS) attacks.

    ```bash
    gcloud compute security-policies create gclb-fw-policy \
    --description "Block XSS attacks"

    gcloud compute security-policies rules create 1000 \
    --security-policy gclb-fw-policy \
    --expression "evaluatePreconfiguredExpr('xss-stable')" \
    --action "deny-403" \
    --description "XSS attack filtering"
    ```



## Configure Multicluster Ingress



56. Create MCI and MCS

    ```bash
    cat <<EOF > ${WORKDIR}/mci.yaml
    apiVersion: networking.gke.io/v1beta1
    kind: MultiClusterIngress
    metadata:
      name: istio-ingressgateway-multicluster-ingress
      namespace: istio-system
      annotations:
        networking.gke.io/static-ip: "${GCLB_IP}"
        networking.gke.io/pre-shared-certs: "${BANK_MANAGED_CERT},${SHOP_MANAGED_CERT},${SIMPLE_MANAGED_CERT}"
    spec:
      template:
        spec:
          backend:
            serviceName: istio-ingressgateway-multicluster-svc
            servicePort: 80
    EOF

    cat <<EOF > ${WORKDIR}/mcs.yaml
    apiVersion: networking.gke.io/v1beta1
    kind: MultiClusterService
    metadata:
      name: istio-ingressgateway-multicluster-svc
      namespace: istio-system
      annotations:
        beta.cloud.google.com/backend-config: '{"ports": {"80":"gke-ingress-config"}}'
    spec:
      template:
        spec:
          selector:
            app: istio-ingressgateway
          ports:
          - name: frontend
            protocol: TCP
            port: 80 # Port the Service listens on
            targetPort: 8080 # Port the Endpoint or Pod listens on
      clusters:
      - link: "${CLUSTER_1_ZONE}/${CLUSTER_1}"
      - link: "${CLUSTER_2_ZONE}/${CLUSTER_2}"
    EOF

    cat <<EOF > ${WORKDIR}/backend-config.yaml
    apiVersion: cloud.google.com/v1beta1
    kind: BackendConfig
    metadata:
      name: gke-ingress-config
      namespace: istio-system
    spec:
      healthCheck:
        type: HTTP
        port: 15021
        requestPath: /healthz/ready
      securityPolicy:
        name: gclb-fw-policy
    EOF
    ```



57. Apply the backendconfig, MCS and MCI manifests.

    ```bash
    kubectl --context ${CLUSTER_INGRESS} apply -f ${WORKDIR}/backend-config.yaml
    kubectl --context ${CLUSTER_INGRESS} apply -f ${WORKDIR}/mcs.yaml
    kubectl --context ${CLUSTER_INGRESS} apply -f ${WORKDIR}/mci.yaml
    ```



58. Wait until you get a GCLB IP and navigate to ensure Online Boutique works. CTRL-C to exit once you have an IP and navigate to the IP address in a browser.

    ```bash
    watch kubectl --context ${CLUSTER_INGRESS} -n istio-system get multiclusteringress -o jsonpath="{.items[].status.VIP}"
    ```



59. Create a `Gateway` resource in the `istio-system` namespace. Creating this in the `istio-system` namespace allows you to use it in other namespaces (see VirtualService below). Gateways are usually owned by the platform admins or network admins team. Therefore, the Gateway resource is created in the `istio-system` namespace owned by the platform admin.

    ```bash
    cat <<EOF > ${WORKDIR}/frontend-gateway.yaml
    apiVersion: networking.istio.io/v1alpha3
    kind: Gateway
    metadata:
      name: frontend-gateway
      namespace: istio-system
    spec:
      selector:
        istio: ingressgateway # use Istio ingress gateway
      servers:
      - port:
          number: 80
          name: http
          protocol: HTTP
        hosts:
        - "${SHOP_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
        - "${BANK_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
        - "${SIMPLE_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    EOF

    kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/frontend-gateway.yaml
    kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/frontend-gateway.yaml
    ```



60. Create VirtualService in the bank, shop, and simple-demo namespaces.

    ```bash
    cat <<EOF > ${WORKDIR}/bank-virtualservice.yaml
    apiVersion: networking.istio.io/v1alpha3
    kind: VirtualService
    metadata:
      name: bank-frontend-virtualservice
      namespace: bank-of-anthos
    spec:
      hosts:
      - "${BANK_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
      gateways:
      - istio-system/frontend-gateway
      http:
        - route:
          - destination:
              host: frontend
              port:
                number: 80
    EOF

    kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/bank-virtualservice.yaml
    kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/bank-virtualservice.yaml
    
    cat <<EOF > ${WORKDIR}/shop-virtualservice.yaml
    apiVersion: networking.istio.io/v1alpha3
    kind: VirtualService
    metadata:
        name: shop-frontend-virtualservice
        namespace: online-boutique
    spec:
        hosts:
        - "${SHOP_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
        gateways:
        - istio-system/frontend-gateway
        http:
        - route:
            - destination:
                host: frontend
                port:
                  number: 80
    EOF

    kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/shop-virtualservice.yaml
    kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/shop-virtualservice.yaml

    cat <<EOF > ${WORKDIR}/simple-virtualservice.yaml
    apiVersion: networking.istio.io/v1alpha3
    kind: VirtualService
    metadata:
        name: simple-frontend-virtualservice
        namespace: simple-demo
    spec:
        hosts:
        - "${SIMPLE_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
        gateways:
        - istio-system/frontend-gateway
        http:
        - route:
            - destination:
                host: whereami-frontend
                port:
                  number: 80
    EOF

    kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/simple-virtualservice.yaml
    kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/simple-virtualservice.yaml
    ```


Note that the VirtualService is created in the application namespace. Typically, the application owner decides and conifgures how and what traffic gets routed to the application so VirtualService is deployed by the app owner. You can also deploy the VirtualServices centrally, for example, i the `istio-system` namespace (owned by the platform admin). Then you can use the `exportTo` value to share the VirtualService to the application namespace. This way you can cerntalize all Ingress control centrally. The decision depends upon your organization and roles and responsibility structure.

61. Setup Locality based routing destination rules for the simple demo virtual services.

    ```bash
    cat <<EOF > ${WORKDIR}/simple-frontend-region1-destrule.yaml
    apiVersion: networking.istio.io/v1beta1
    kind: DestinationRule
    metadata:
      name: simple-frontend-destrule
      namespace: simple-demo
    spec:
      host: whereami-frontend.simple-demo.svc.cluster.local
      trafficPolicy:
        connectionPool:
          http:
            maxRequestsPerConnection: 1
        loadBalancer:
          simple: ROUND_ROBIN
          localityLbSetting:
            enabled: true
            failover:
              - from: ${CLUSTER_1_ZONE}
                to: ${CLUSTER_2_ZONE}
        outlierDetection:
          consecutive5xxErrors: 1
          interval: 1s
          baseEjectionTime: 1m
    EOF

    cat <<EOF > ${WORKDIR}/simple-frontend-region2-destrule.yaml
    apiVersion: networking.istio.io/v1beta1
    kind: DestinationRule
    metadata:
      name: simple-frontend-destrule
      namespace: simple-demo
    spec:
      host: whereami-frontend.simple-demo.svc.cluster.local
      trafficPolicy:
        connectionPool:
          http:
            maxRequestsPerConnection: 1
        loadBalancer:
          simple: ROUND_ROBIN
          localityLbSetting:
            enabled: true
            failover:
              - from: ${CLUSTER_2_ZONE}
                to: ${CLUSTER_1_ZONE}
        outlierDetection:
          consecutive5xxErrors: 1
          interval: 1s
          baseEjectionTime: 1m
    EOF

    cat <<EOF > ${WORKDIR}/simple-backend-region1-destrule.yaml
    apiVersion: networking.istio.io/v1beta1
    kind: DestinationRule
    metadata:
      name: simple-backend-destrule
      namespace: simple-demo
    spec:
      host: whereami-backend.simple-demo.svc.cluster.local
      trafficPolicy:
        connectionPool:
          http:
            maxRequestsPerConnection: 1
        loadBalancer:
          simple: ROUND_ROBIN
          localityLbSetting:
            enabled: true
            failover:
              - from: ${CLUSTER_1_ZONE}
                to: ${CLUSTER_2_ZONE}
        outlierDetection:
          consecutive5xxErrors: 1
          interval: 1s
          baseEjectionTime: 1m
    EOF

    cat <<EOF > ${WORKDIR}/simple-backend-region2-destrule.yaml
    apiVersion: networking.istio.io/v1beta1
    kind: DestinationRule
    metadata:
      name: simple-backend-destrule
      namespace: simple-demo
    spec:
      host: whereami-backend.simple-demo.svc.cluster.local
      trafficPolicy:
        connectionPool:
          http:
            maxRequestsPerConnection: 1
        loadBalancer:
          simple: ROUND_ROBIN
          localityLbSetting:
            enabled: true
            failover:
              - from: ${CLUSTER_2_ZONE}
                to: ${CLUSTER_1_ZONE}
        outlierDetection:
          consecutive5xxErrors: 1
          interval: 1s
          baseEjectionTime: 1m
    EOF

    kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/simple-frontend-region1-destrule.yaml
    kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/simple-backend-region1-destrule.yaml
    kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/simple-frontend-region2-destrule.yaml
    kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/simple-backend-region2-destrule.yaml
    ```



## Access applications

You can access both `bank` and `shop` applications once the SSL certificates have been provisioned.



62. Check Bank of Anthos managed certificate status.

    ```bash
    kubectl --context ${CLUSTER_INGRESS} -n istio-system get managedcertificate bank-managed-cert -ojsonpath='{.status.certificateStatus}'
    kubectl --context ${CLUSTER_INGRESS} -n istio-system get managedcertificate shop-managed-cert -ojsonpath='{.status.certificateStatus}'
    kubectl --context ${CLUSTER_INGRESS} -n istio-system get managedcertificate simple-managed-cert -ojsonpath='{.status.certificateStatus}'
    ```

The output is similar to the following:


    # Bank managed cert
    Active

    # Shop managed cert
    Active

    # Simple managed cert
    Active



63. You can also check the certificate status via `gcloud`.

    ```bash
    gcloud beta compute ssl-certificates list
    ```

The output is similar to the following: 


    mcrt-bdc70848-db4c-4911-b1f2-e1b3832d496c  MANAGED  2021-03-20T13:38:44.407-07:00  2021-06-18T13:38:46.000-07:00  ACTIVE
    ob1.endpoints.asm-multi-environment-cluster.cloud.goog: ACTIVE
    mcrt-f90cd39f-c706-4e51-9835-8493833ec2f8  MANAGED  2021-03-20T13:38:43.705-07:00  2021-06-18T12:53:51.000-07:00  ACTIVE
    boa1.endpoints.asm-multi-environment-cluster.cloud.goog: ACTIVE



64. You can access the applications via the Cloud Endpoints DNS name.

    ```bash
    echo -e "https://${BANK_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    echo -e "https://${SHOP_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    echo -e "https://${SIMPLE_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    ```


Navigate through both applications. They should be fully functional.



## Cleaning up



65. Delete MCI.

    ```bash
    kubectl --context ${CLUSTER_INGRESS} -n istio-system delete -f ${WORKDIR}/mci.yaml
    kubectl --context ${CLUSTER_INGRESS} -n istio-system delete -f ${WORKDIR}/mcs.yaml
    kubectl --context ${CLUSTER_INGRESS} -n istio-system delete -f ${WORKDIR}/backend-config.yaml
    ```



66. Delete Cloud Endpoint Services and managed certificates.

    ```bash
    gcloud endpoints services delete ${BANK_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog --async --quiet
    gcloud endpoints services delete ${SHOP_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog --async --quiet
    gcloud endpoints services delete ${SIMPLE_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog --async --quiet
    kubectl --context ${CLUSTER_INGRESS} -n istio-system delete -f ${WORKDIR}/bank-managed-cert.yaml
    kubectl --context ${CLUSTER_INGRESS} -n istio-system delete -f ${WORKDIR}/shop-managed-cert.yaml
    kubectl --context ${CLUSTER_INGRESS} -n istio-system delete -f ${WORKDIR}/simple-managed-cert.yaml
    ```



67. Verify that the managed certificates are deleted.

    ```bash
    gcloud compute ssl-certificates list
    ```

The output is similar to the following: 


    Listed 0 items.




68. Delete Google Service Account created for Workload identity.

    ```bash
    gcloud iam service-accounts remove-iam-policy-binding \
    --role roles/iam.workloadIdentityUser \
    --member "serviceAccount:${PROJECT_ID}.svc.id.goog[bank-of-anthos/${KSA_NAME}]" \
    ${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com
    gcloud iam service-accounts delete ${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com --quiet
    ```



69. Delete hub registration.

    ```bash
    gcloud container hub memberships delete ${CLUSTER_INGRESS} --quiet
    gcloud container hub memberships delete ${CLUSTER_1} --quiet
    gcloud container hub memberships delete ${CLUSTER_2} --quiet
    ```



70. Delete the clusters.

    ```bash
    gcloud container clusters delete ${CLUSTER_INGRESS} --zone ${CLUSTER_INGRESS_ZONE} --async --quiet
    gcloud container clusters delete ${CLUSTER_1} --zone ${CLUSTER_1_ZONE} --async --quiet
    gcloud container clusters delete ${CLUSTER_2} --zone ${CLUSTER_2_ZONE} --async --quiet
    ```



71. Delete the GCLB IP.

    ```bash
    gcloud compute addresses delete gclb-ip --global --quiet
    ```


72. Delete Cloud Armor Security policy.

    ```bash
    gcloud compute security-policies delete gclb-fw-policy --quiet
    ```



73. Confirm endpoints, certificates, hub registration and clusters are deleted.

    ```bash
    gcloud endpoints services list
    gcloud compute security-policies list
    gcloud compute ssl-certificates list
    gcloud container hub memberships list
    gcloud container clusters list
    ```

The output is similar to the following:



    Listed 0 items.



74. Unset KUBECONFIG

    ```bash
    unset KUBECONFIG
    ```


75. Delete WORKDIR

    ```bash
    cd $HOME && rm -rf $WORKDIR
    ```
